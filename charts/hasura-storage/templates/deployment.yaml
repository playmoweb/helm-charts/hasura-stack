apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "hasura-storage.fullname" . }}
  labels:
    {{- include "hasura-storage.labels" . | nindent 4 }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "hasura-storage.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      {{- with .Values.podAnnotations }}
      annotations:
        checksum/configmap: {{ include (print $.Template.BasePath "/configmap.yaml") . | sha256sum }}
        checksum/secret: {{ include (print $.Template.BasePath "/secret.yaml") . | sha256sum }}
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "hasura-storage.selectorLabels" . | nindent 8 }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "hasura-storage.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      initContainers:
        - name: wait-hasura
          image: busybox
          command:
            - 'sh'
            - '-c'
            - |
              endpoint=$HASURA_ENDPOINT
              goodEndpoint=${endpoint::-2}
              until wget 2>/dev/null -q --spider $goodEndpoint/healthz
              do
                echo \"waiting for hasura\"
                sleep 1
              done
          envFrom:
            - configMapRef:
                name: {{ include "hasura-storage.fullname" . }}
            - secretRef:
                name: {{ include "hasura-storage.fullname" . }}
        - name: update-postgresql
          image: jbergknoff/postgresql-client
          args: [ "$(POSTGRES_MIGRATIONS_SOURCE)" ]
          command:
            - "psql"
            - "-c"
            - |
              CREATE SCHEMA IF NOT EXISTS storage;
              CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;
              CREATE EXTENSION IF NOT EXISTS citext WITH SCHEMA public;
              CREATE OR REPLACE FUNCTION public.set_current_timestamp_updated_at() RETURNS trigger LANGUAGE plpgsql AS '
              declare _new record;
              begin _new := new;
              _new."updated_at" = now();
              return _new;
              end;
              ';
          envFrom:
            - configMapRef:
                name: {{ include "hasura-storage.fullname" . }}
            - secretRef:
                name: {{ include "hasura-storage.fullname" . }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          args: ["serve"]
          envFrom:
            - configMapRef:
                name: {{ include "hasura-storage.fullname" . }}
            - secretRef:
                name: {{ include "hasura-storage.fullname" . }}
          ports:
            - name: http
              containerPort: 8000
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /healthz
              port: http
          readinessProbe:
            httpGet:
              path: /healthz
              port: http
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
